import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
// import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) {
        setTimeout(fn, 1000);
    }

    ngOnInit(): void {
        // this.doLater(() =>
        //     dialogs.action("Mensaje", "Cancelar!", ["Opcion1","Opcion2"])
        //         .then((results) => {
        //             console.log("resultados: " + results);
        //             if (results === "Opcion1") {
        //                 this.doLater(() =>
        //                     dialogs.alert({
        //                         title: "Titulo 1 ",
        //                         message: "mensaje 1",
        //                         okButtonText: "botón 1"
        //                     }).then(() => console.log("Cerrado 1!"))
        //                 );
        //             }
        //             if (results === "Opcion2") {
        //                 this.doLater(() =>
        //                     dialogs.alert({
        //                         title: "Titulo 2 ",
        //                         message: "mensaje 2",
        //                         okButtonText: "botón 2"
        //                     }).then(() => console.log("Cerrado 2!"))
        //                 );
        //             }
        //         })
        // );
        const toastOptions: Toast.ToastOptions = {text: "Hola Mundo", duration: Toast.DURATION.SHORT};
        this.doLater(() => Toast.show(toastOptions));
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
