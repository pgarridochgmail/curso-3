import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ConfiguracionComponent } from "./configuracion.component";
import { EspecificaComponent } from "./especifica/especifica.component";

const routes: Routes = [
    { path: "", component: ConfiguracionComponent},
    { path: "configuracion", component: ConfiguracionComponent },
    { path: "especifica", component: EspecificaComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ConfiguracionRoutingModule { }
