import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { RouterExtensions } from "nativescript-angular/router";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "especifica",
    templateUrl: "./especifica.component.html"
})
export class EspecificaComponent implements OnInit {

    nombreUsuario: string = "";

    constructor(private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        console.log("Inició Configuración Específica");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    modificarNombreUsuario(nuevoNombre: string) {
        this.nombreUsuario = nuevoNombre;
        appSettings.setString("nombreUsuario", this.nombreUsuario);
        this.routerExtensions.navigate(['configuracion']);
    }
}
