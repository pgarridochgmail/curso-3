import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ConfiguracionRoutingModule } from "./configuracion-routing.module";
import { ConfiguracionComponent } from "./configuracion.component";
import { EspecificaComponent } from "./especifica/especifica.component";
import { ModificarComponent } from "~/app/configuracion/modificar.component";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ConfiguracionRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        ConfiguracionComponent,
        EspecificaComponent,
        ModificarComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ConfiguracionModule { }
