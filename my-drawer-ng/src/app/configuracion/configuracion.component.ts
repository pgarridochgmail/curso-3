import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { RouterExtensions } from "nativescript-angular/router";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "general",
    templateUrl: "./configuracion.component.html"
})
export class ConfiguracionComponent implements OnInit {

    nombreUsuario: string = "";

    constructor(private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        console.log("Inició Configuración General");
        this.nombreUsuario = appSettings.getString("nombreUsuario");
        if (this.nombreUsuario === "") { this.nombreUsuario = "No registrado"; }
        console.dir(this.nombreUsuario);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

}
