import { Injectable } from "@angular/core";
import { Opcion } from "~/app/models/opcion";

@Injectable({
  providedIn: "root"
})
export class OpcionesService {
  private opciones: Array<Opcion> = [];

  constructor() {
    this.crearOpciones(3);
  }

  crearOpciones(cantidad: number) {
      let opcion: Opcion;
      for (let i = 1; i <= cantidad; i++) {
          opcion = new Opcion();
          opcion.id = i;
          opcion.nombre = "Hotel " + i;
          opcion.categoria = "Sin Categoria";
          this.opciones.push(opcion);
      }
  }

  agregar() {
      const opcion: Opcion = new Opcion();
      let i: number = 0;
      this.opciones.forEach((o) => {
          if (i < o.id) {
              i = o.id;
          }
      });
      i++;
      opcion.id = i;
      opcion.nombre = "Hotel " + i;
      opcion.categoria = "Sin Categoria";
      this.opciones.push(opcion);
  }

  listar() {
      return this.opciones;
  }

  actualizar(opc: Opcion): boolean {
      const indice = this.opciones.findIndex((o) => o.id === opc.id);
      if (indice >= 0) {
          this.opciones[indice] = opc;

          return true;
      } else {
          return false;
      }
  }

  eliminar(opc: Opcion) {
      this.opciones = this.opciones.filter((o) => o.id !== opc.id);
  }

  buscar(s: string) {
      return this.opciones.filter((o) => o.nombre.includes(s));
  }

  buscarPoId(id: number) {
      const coincidencias = this.opciones.filter((o) => o.id.toString() === id.toString());
      if (coincidencias.length > 0) {
        return coincidencias[0];
      }
  }

}
