import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { SearchRoutingModule } from "./search-routing.module";
import { SearchComponent } from "./search.component";
import { CommonModule } from "@angular/common";
import { SearchFormComponent } from "./search-form.component";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    SearchRoutingModule,
    CommonModule,
    NativeScriptFormsModule
  ],
  declarations: [
    SearchComponent,
    SearchFormComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class SearchModule { }
