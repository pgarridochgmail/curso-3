import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as Toast from "nativescript-toasts";

@Component({
  selector: 'SearchForm',
  template: `
    <TextField [(ngModel)]="textFieldValue" hint="Enter text..."></TextField>
    <Button text="Search" (tap)="onButtonTap()"></Button>
  `
})
export class SearchFormComponent implements OnInit {

    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter<string>();
    @Input() inicial: string;

  constructor() { }

  ngOnInit(): void {
      this.textFieldValue = this.inicial;
  }

  onButtonTap(): void {
    console.log(this.textFieldValue);
    if (this.textFieldValue.length > 2) {
        this.search.emit(this.textFieldValue);
    } else {
        Toast.show({text: "Se requiere mínimo 3 caractéres", duration: Toast.DURATION.SHORT});
    }
  }
}
