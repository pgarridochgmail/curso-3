import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts";
import { OpcionesService } from "~/app/domain/opciones.service";
import { Opcion } from "~/app/models/opcion";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
  selector: "ns-detalle",
  templateUrl: "./detalle.component.html"
})
export class DetalleComponent implements OnInit {

    opcion: Opcion;
    id: number;

    constructor(private opcionesService: OpcionesService,
                private activatedRoute: ActivatedRoute,
                private routerExtensions: RouterExtensions
    ) {
        this.id = this.activatedRoute.snapshot.params['id'];
        // console.log(this.id);
        this.opcion = this.opcionesService.buscarPoId(this.id);
        // console.dir(this.opcion);
    }
    ngOnInit(): void {
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    categoria() {
        const actionOptions = {
            title: "Modificar Categoría",
            message: "Elija la nueva categoría",
            cancelButtonText: "Cancelar",
            actions: ["1 Estrella", "2 Estrellas", "3 Estrellas", "4 Estrellas", "5 Estrelllas"],
            cancelable: true // Android only
        };
        dialogs.action(actionOptions)
            .then((nuevaCategoria) => {
                console.log("resultados: " + nuevaCategoria);
                if (nuevaCategoria) {
                    this.opcion.categoria = nuevaCategoria;
                    this.opcionesService.actualizar(this.opcion);
                    const toastOptions: Toast.ToastOptions = {
                        text: "Se actualizó la categoría a: " + nuevaCategoria,
                        duration: Toast.DURATION.SHORT
                    };
                    Toast.show(toastOptions);
                }
            });
    }

    eliminar() {
        const confirmOptions = {
            title: "Eliminar opciones",
            message: "¿Está seguro que desea eliminar " + this.opcion.nombre + "?",
            okButtonText: "Sí",
            cancelButtonText: "No"
        };
        dialogs.confirm(confirmOptions)
            .then((elimino) => {
                console.log("resultados: " + elimino);
                if (elimino) {
                    // this.opciones = this.opciones.filter((o) => o.id !== x.id);
                    dialogs.alert({
                        title: "Eliminación de Opciones",
                        message: "Se eliminó " + this.opcion.nombre,
                        okButtonText: "Cerrar"
                    }).then(() => {
                            this.opcionesService.eliminar(this.opcion);
                            console.log("Se eliminó " + this.opcion.nombre);
                            this.regresar();
                        }
                    );
                }
            });
    }

    regresar() {
        this.routerExtensions.navigate(['listado']);
    }

    onLongPress() {
        this.categoria();
    }

}
